import React, {createContext} from 'react';
import './App.css';
import ClientPlattform from "./screens/ClientPlattform";
import {HashRouter as Router} from 'react-router-dom';
import keycloak from "./keycloak";
import {ReactKeycloakProvider} from '@react-keycloak/web';
import StudentApi from "./api/StudentApi";

export const StudentApiContext = createContext(new StudentApi(null));

function App() {

    return (
        <ReactKeycloakProvider authClient={keycloak}>
            <StudentApiContext.Provider value={new StudentApi(keycloak)}>
                <Router>
                    <ClientPlattform/>
                </Router>
            </StudentApiContext.Provider>
        </ReactKeycloakProvider>
    );
}

export default App;
