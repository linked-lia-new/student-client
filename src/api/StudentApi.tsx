import React from 'react'
import Keycloak from "keycloak-js";
import StudentEducation from "../models/StudentEducation";
import StudentExperience from "../models/StudentExperience";
import StudentSkill from '../models/StudentSkill';

export default class StudentApi {

    keycloak: any;

    constructor(keycloak: any) {
        this.keycloak = keycloak;
    }

    getAll = async () => {
        const response = await fetch('http://localhost:8088/students');
        const result = await response.json();
        return result;
    }

    getEducationListFromStudent = async () : Promise<StudentEducation[]> => {
        if(!this.keycloak){
            throw "NOPE";
        }
        const response = await fetch ("http://localhost:8088/educations/alleducations",
            {
                method: 'GET',
                headers: {"Content-type": "application/json", "Authorization": "Bearer " + this.keycloak.token},
            });

        return await response.json();
    }
    getSkillListFromStudent = async () : Promise<StudentSkill[]> => {
        if(!this.keycloak){
            throw "NOPE";
        }
        const response = await fetch ("http://localhost:8088/skills/get_all_skills",
            {
                method: 'GET',
                headers: {"Content-type": "application/json", "Authorization": "Bearer " + this.keycloak.token},
            });

        return await response.json();
    }

    getExperienceListFromStudent = async () : Promise<StudentExperience[]> => {
        if(!this.keycloak){
            throw "NOPE";
        }
        const response = await fetch ("http://localhost:8088/experiences/allexperience",
            {
                method: 'GET',
                headers: {"Content-type": "application/json", "Authorization": "Bearer " + this.keycloak.token},
            });

        return await response.json();
    }

    public addStudent = async (data = {}) => {
        if(!this.keycloak){
            throw "NOPE";
        }
        const response = await fetch('http://localhost:8088/students/addstudent',
            {
                method: 'POST',
                headers: {"Content-type": "application/json", "Authorization": "Bearer " + this.keycloak.token},
                body: JSON.stringify(data)
            });

        console.log("addStudent TOKENET " + this.keycloak.token);
        return await response.json();
    }

    addEducation = async (data = {}) => {
        const response = await fetch("http://localhost:8088/educations/addeducation",
            {
                method: 'POST',
                headers: {"Content-type": "application/json", "Authorization": "Bearer " + this.keycloak.token},
                body: JSON.stringify(data)
            });
        return await response.json();
    }

    addExperience = async (data = {}) => {
        const response = await fetch("http://localhost:8088/experiences/addExp",
            {
                method: 'POST',
                headers: {"Content-type": "application/json", "Authorization": "Bearer " + this.keycloak.token},
                body: JSON.stringify(data)
            });
        return await response.json();
    }

    deleteExperience = async (id: string, data = {}) => {
        const response = await fetch("http://localhost:8088/experiences/" + id + "/delete",
            {
                method: 'DELETE',
                headers: {"Content-type": "application/json", "Authorization": "Bearer " + this.keycloak.token},
                //headers: {"Content-type": "application/json"},
                //body: JSON.stringify(data)
            });
        //console.log("DELETE", response.json());
        return await response.json();
    }

    deleteEducation = async (id: string, data = {}) => {
        const response = await fetch("http://localhost:8088/educations/" + id + "/delete",
            {
                method: 'DELETE',
                headers: {"Content-type": "application/json", "Authorization": "Bearer " + this.keycloak.token},
                //headers: {"Content-type": "application/json"},
                //body: JSON.stringify(data)
            });
        //console.log("DELETE", response.json());
        return await response.json();
    }

    addSkill = async (data = {}) => {
        const response = await fetch("http://localhost:8088/skills/add_skill",
            {
                method: 'POST',
                headers: {"Content-type": "application/json", "Authorization": "Bearer " + this.keycloak.token},
                body: JSON.stringify(data)
            });
        const result = await response.json();
        return result;
    }

    deleteSkill = async (id: string, data = {}) => {
        const response = await fetch("http://localhost:8088/skills/" + id + "/delete_skill",
            {
                method: 'DELETE',
                headers: {"Content-type": "application/json", "Authorization": "Bearer " + this.keycloak.token},
                //headers: {"Content-type": "application/json"},
                //body: JSON.stringify(data)
            });
        //console.log("DELETE", response.json());
        return await response.json();
    }

    addStudentPic = async (id: string, data: any) => {
        const response = await fetch('http://localhost:8088/students/' + id,
            {
                method: 'POST',
                //headers: {"Content-type": "multipart/form-data"},
                body: data//JSON.stringify(data)
            });
        const result = await response.json();
        return result;
    }

   updateStudent = async (data: any) => {
        const response = await fetch('http://localhost:8088/students/update',
            {
                method: 'POST',
                headers: {"Content-type": "application/json", "Authorization": "Bearer " + this.keycloak.token},
                body: JSON.stringify(data)
            });
        return await response.json();
    }

}

