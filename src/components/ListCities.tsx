import React from 'react';

export default function ListCities() {
    const cities = [
        {
            label: 'Arninge'
        },
        {
            label: 'Stockholm'
        },
        {
            label: 'Malmo'
        },
        {
            label: 'Lund'
        },
        {
            label: 'Växjö'
        },
        {
            label: 'Uppsala'
        },
        {
            label: 'Gotland'
        },
    ];
    return cities;
}


