import React from 'react';
import {useKeycloak} from '@react-keycloak/web';

export default function LogoutComponent() {
    const {keycloak, initialized} = useKeycloak();
    console.log("LogoutComponent")
    if (keycloak.authenticated)
        keycloak.logout()
    else
        window.location.href = "http://localhost:3000";

    return null;
}
