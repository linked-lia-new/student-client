import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faComments, faEye, faHandshake, faInfo, faUser} from "@fortawesome/free-solid-svg-icons";
import {Tooltip, Zoom} from "@material-ui/core";
import {Link} from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        navStyle: {
            background: '#c7d8c6'
        },
        title: {
            flexGrow: 1,
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
        },
        icon: {
            color: '#ffff'
        },
        item: {
            margin: 8
        },
    })
);

export default function Navbar() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar className={classes.navStyle}>
                    <div className={classes.title}>
                        <Tooltip TransitionComponent={Zoom} title="Matchningar">
                            <Link to={'/matches'}>
                                <IconButton className={classes.icon}>
                        <span className="fa-layers fa-fw">
                            <FontAwesomeIcon icon={faHandshake}/>
                            <span className="fa-layers-counter">3</span>
                        </span>
                                </IconButton>
                            </Link>
                        </Tooltip>
                        <Tooltip TransitionComponent={Zoom} title="Meddelanden">
                            <Link to={'/messages'}>
                                <IconButton className={classes.icon}>
                                    <FontAwesomeIcon icon={faComments}/>
                                </IconButton>
                            </Link>
                        </Tooltip>
                        <Tooltip TransitionComponent={Zoom} title="Besökare">
                            <Link to={'/visitors'}>
                                <IconButton className={classes.icon}>
                                    <FontAwesomeIcon icon={faEye}/>
                                </IconButton>
                            </Link>
                        </Tooltip>
                        <Tooltip TransitionComponent={Zoom} title="Hjälp">
                            <Link to={'/studentProfile'}>
                                <IconButton className={classes.icon}>
                                    <FontAwesomeIcon icon={faInfo}/>
                                </IconButton>
                            </Link>
                        </Tooltip>
                    </div>
                    <Tooltip TransitionComponent={Zoom} title="ClientPlattform">
                        <Link to={'/profile'}>
                            <IconButton className={classes.icon}>
                                <FontAwesomeIcon icon={faUser}/>
                            </IconButton>
                        </Link>
                    </Tooltip>
                </Toolbar>
            </AppBar>
        </div>
    )
        ;
}
