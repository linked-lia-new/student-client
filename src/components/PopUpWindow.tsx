import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    paper: {
        backgroundColor: "#eeedec"
    },
    title: {
        backgroundColor: "#ccc8c6",
        color: "#eeedec"
    },
    closeButton: {
        backgroundColor: "#ccc8c6"
    }
}));

export default function PopupWindow(props: any) {
    const classes = useStyles();
    const {children, openWindow, closeWindow} = props;

    return (
        <div>
            <Dialog
                open={openWindow}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                PaperProps={{classes:{root: classes.paper}}}
            >
                <DialogTitle className={classes.title} id="alert-dialog-title">
                    Lägg till
                </DialogTitle>
                <DialogContent>
                    {children}
                </DialogContent>
                <DialogActions className={classes.closeButton}>
                    <Button onClick={closeWindow} className={classes.paper}>
                        Stäng
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
