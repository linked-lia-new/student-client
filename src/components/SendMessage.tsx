import React, {ChangeEvent, useContext, useState} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Paper, TextField} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";
import {StudentApiContext} from "../App";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            maxHeight: "80vh",
            minHeight: "20vh",
            minWidth: "20vw",
            width: "80%",
            margin: "20px",
            background: "#c7d8c6",
            justifyContent: "center"
        },
        form: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                justifyContent: "center"
            }
        },
        formDesign: {
            display: "flex",
            color: "#eeedec"
        },
        textField: {
            width: "100%",
            height: "100%",
            color: "#eeedec"
        },
        submitButton: {
            marginTop: "40%",
            color: "#eeedec"
        }
    }),
);

type FormData = {
    message: string;
};

export default function SendMessage() {
    const classes = useStyles();
    const {register, setValue, handleSubmit, formState: {errors}} = useForm<FormData>();
    const [message, setMessage] = useState("");

    const handleMessageChange = (event: ChangeEvent<HTMLInputElement>) => {
        let newMessage = event.target.value;
        setMessage(newMessage);
    }

    return (
            <Paper elevation={20} className={classes.root}>
                <form className={classes.form} noValidate autoComplete="off">
                    <div className={classes.formDesign}>
                        <TextField {...register("message")}
                                   id="outlined-name-input"
                                   label="Meddelande"
                                   value={message}
                                   variant="outlined"
                                   className={classes.textField}
                                   required
                                   onChange={handleMessageChange}
                        />
                    </div>
                    <Button type="submit" className={classes.submitButton}>
                        SKICKA
                    </Button>
                </form>
            </Paper>
    )
}
