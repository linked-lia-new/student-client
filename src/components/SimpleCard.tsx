import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Card, Typography} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        margin: "10px",
        padding: "5px",
        width: "90%",
        justifyContent: "space-between",
        backgroundColor: "#c7d8c6",
        color: "#726e6e"
    }
}));

export default function SimpleCard(props: any) {
    const classes = useStyles();
    const {school, field, degree, city, startDate, endDate} = props;

    return (
        <Card elevation={5} className={classes.root}>
            <div>
                <Typography variant="h6" gutterBottom>
                    {school}
                </Typography>
                <Typography variant="subtitle1" gutterBottom>
                    {field}
                </Typography>
                <Typography variant="body2" gutterBottom component="div">
                    {degree}
                </Typography>
                <Typography variant="body2" gutterBottom component="div">
                    {city}
                </Typography>
                <Typography variant="body2" gutterBottom component="div">
                    {startDate}--{endDate}
                </Typography>
            </div>
        </Card>
    )
}
