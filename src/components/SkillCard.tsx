import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Card, Typography} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        margin: "10px",
        width: "90%",
        padding: "5px",
        justifyContent: "space-between",
        backgroundColor: "#c7d8c6",
        color: "#726e6e"

    }
}));

export default function SkillCard(props: any) {
    const classes = useStyles();
    const {skill, level} = props;

    return (
        <Card elevation={5} className={classes.root}>
            <div>
                <Typography variant="subtitle1" gutterBottom>
                    {skill}
                </Typography>
                <Typography variant="body2" gutterBottom>
                    {level}
                </Typography>
            </div>
        </Card>
    )
}
