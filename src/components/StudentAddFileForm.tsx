import React, {ChangeEvent} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Paper, TextField} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            backgroundColor: "#c7d8c6",
            margin: "20px"
        },
        form: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                width: '25ch'
            }
        },
        saveButton: {
            color: "#eeedec"
        }
    })
);
type FormData = {
    file: string;
};

export default function StudentAddFileForm() {
    const classes = useStyles();
    const {register, handleSubmit, formState: {errors}} = useForm<FormData>();
    const [file, setFile] = React.useState<File>();

    const handleFileChange = (event: ChangeEvent<HTMLInputElement>) => {
        let fileList: any = event.target.files;
        if (!fileList)
            return null;
        setFile(fileList[0]);
        console.log(fileList[0]);
    }

    const onSubmit = handleSubmit((data) => {
        const formData = new FormData();
        if (file) {
            formData.append("file", file);
        }
        console.log(formData);
    });

    return (
        <Paper elevation={20} className={classes.root}>
            <form className={classes.form} encType="multipart/form-data" onSubmit={onSubmit}>
                <TextField {...register("file")}
                           type="file"
                           id="outlined-lastname-input"
                           variant="outlined"
                           style={{width: "85%"}}
                           required
                           onChange={handleFileChange}
                />
                <Button className={classes.saveButton} type="submit">
                    Spara
                </Button>
            </form>
        </Paper>
    )
}
