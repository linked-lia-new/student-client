import React from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Grid} from "@material-ui/core";
import StudentBack from '../assets/media/StudentBack.png';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            justifyContent: 'center',
            height: '9rem',
        },
        img: {
            flex: 1,
            width: '100%',
            height: '15rem',
            objectFit: 'cover',
        },
        item: {
            justifyItem: 'center'
        }
    })
);

export default function StudentBackground() {
    const classes = useStyles();

    return (
        <Grid container className={classes.root}>
            <Grid item className={classes.item} xs={12} md={10}>
                <img
                    className={classes.img}
                    src={StudentBack}
                    alt={'pic'}
                />
            </Grid>
        </Grid>
    );
}
