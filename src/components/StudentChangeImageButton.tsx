import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import {CardActionArea, Grid, Paper} from "@material-ui/core";
import {Typography} from "@mui/material";
import PopUpWindow from "./PopUpWindow";
import StudentAddFileForm from "./StudentAddFileForm";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'grid',
            '& > *': {
                margin: theme.spacing(3)
            }
        },
        position: {
            justifyContent: 'center',
            [theme.breakpoints.up('sm')]: {
                justifyContent: 'flex-start',
                marginLeft: 65
            }
        },
        changePicButton: {
            color: '#726e6e',
            background: '#efd9c1',
            padding: 4,
            marginBottom: 8
        }
    })
);

export default function StudentChangeImageButton() {
    const classes = useStyles();
    const [openWindow, setOpenWindow] = React.useState(false);

    const closeWindow = () => {
        setOpenWindow(false);
    }

    return (
        <Grid container className={classes.position}>
            <Grid item lg={1} md={1} sm={1}/>
            <Typography onClick={() => setOpenWindow(true)}>
                <CardActionArea>
                    <Paper className={classes.changePicButton}>
                        Byt bild
                    </Paper>
                </CardActionArea>
            </Typography>
            <PopUpWindow openWindow={openWindow} closeWindow={closeWindow}>
                <StudentAddFileForm/>
            </PopUpWindow>
        </Grid>
    );
}
