import React, {ChangeEvent, useContext, useState} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Paper, TextField} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";
import {StudentApiContext} from "../App";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            minHeight: "38vh",
            width: "80%",
            margin: "20px",
            background: "#c7d8c6",
            justifyContent: "center"
        },
        form: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                width: '25ch',
            },
        },
        formDesign: {
            display: "flex",
            justifyContent: "around"
        },
        container: {
            textAlign: "right"
        },
        submitButton: {
            color: "#eeedec",
            marginTop: "10px"
        }
    })
);

type FormData = {
    school: string;
    degree: string;
    field: string;
    city: string;
    startDate: string;
    endDate: string;
};


export default function StudentEducationForm() {
    const classes = useStyles();
    const {register, handleSubmit, formState: {errors}} = useForm<FormData>();

    const [school, setSchool] = useState("");
    const [degree, setDegree] = useState("");
    const [field, setField] = useState("");
    const [city, setCity] = useState("");
    const [start, setStart] = useState("");
    const [end, setEnd] = useState("");
    const studentApi = useContext(StudentApiContext);

    const handleSchoolChange = (event: ChangeEvent<HTMLInputElement>) => {
        let school = event.target.value;
        setSchool(school);
    }
    const handleDegreeChange = (event: ChangeEvent<HTMLInputElement>) => {
        let degree = event.target.value;
        setDegree(degree);
    }
    const handleCityChange = (event: ChangeEvent<HTMLInputElement>) => {
        let city = event.target.value;
        setCity(city);
    }
    const handleFieldChange = (event: ChangeEvent<HTMLInputElement>) => {
        let field = event.target.value;
        setField(field);
    }
    const handleStartChange = (event: ChangeEvent<HTMLInputElement>) => {
        let startDate = event.target.value;
        setStart(startDate);
    }
    const handleEndChange = (event: ChangeEvent<HTMLInputElement>) => {
        let endDate = event.target.value;
        setEnd(endDate);
    }

    const onSubmit = handleSubmit((data) => {
        studentApi.addEducation({
            school: school,
            degree: degree,
            city: city,
            field: field,
            startDate: start,
            endDate: end
        })
            .then(res => console.log(res))
            .catch(err => alert(err))
    });

    return (
        <Paper elevation={20} className={classes.root}>
            <form className={classes.form} noValidate autoComplete="off" onSubmit={onSubmit}>
                <TextField {...register("school")}
                           size="small"
                           id="outlined-name-input"
                           label="Skola"
                           value={school}
                           variant="outlined"
                           style={{width: "46%"}}
                           required
                           onChange={handleSchoolChange}
                />
                <TextField {...register("degree")}
                           size="small"
                           id="outlined-lastname-input"
                           label="Utbildning"
                           value={degree}
                           variant="outlined"
                           style={{width: "46%"}}
                           required
                           onChange={handleDegreeChange}
                />
                <TextField {...register("field")}
                           size="small"
                           id="outlined-lastname-input"
                           label="Inriktning"
                           value={field}
                           variant="outlined"
                           style={{width: "46%"}}
                           required
                           onChange={handleFieldChange}
                />
                <TextField {...register("city")}
                           size="small"
                           id="outlined-lastname-input"
                           label="Stad"
                           value={city}
                           variant="outlined"
                           style={{width: "46%"}}
                           required
                           onChange={handleCityChange}
                />
                <TextField {...register("startDate")}
                           size="small"
                           id="outlined-lastname-input"
                           label="Från"
                           value={start}
                           variant="outlined"
                           style={{width: "46%"}}
                           required
                           onChange={handleStartChange}
                />
                <TextField {...register("endDate")}
                           size="small"
                           id="outlined-lastname-input"
                           label="Till"
                           value={end}
                           variant="outlined"
                           style={{width: "46%"}}
                           required
                           onChange={handleEndChange}
                />
                <div className={classes.container}>
                    <Button type="submit" className={classes.submitButton}>
                        LÄGG TILL
                    </Button>
                </div>
            </form>
        </Paper>
    )
}
