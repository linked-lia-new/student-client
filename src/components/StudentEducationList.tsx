import React, {useContext, useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Card} from "@material-ui/core";
import IconButton from "@mui/material/IconButton";
import AddRoundedIcon from '@mui/icons-material/AddRounded';
import {StudentApiContext} from "../App";
import StudentEducation from "../models/StudentEducation";
import DeleteIcon from "@mui/icons-material/Delete";
import SimpleCard from "./SimpleCard";

const useStyles = makeStyles((theme) => ({
    root: {
        margin: "20px",
        padding: "10px",
        backgroundColor: "#efd9c1"
    },
    buttons: {
        backgroundColor: "#ddb6a2",
        marginRight: "5px",
        borderRadius: "50%",
        boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
    }
}));

export default function StudentEducationList() {
    const classes = useStyles();
    const [openWindow, setOpenWindow] = React.useState(false);
    const [reloadList, setReloadList] = React.useState(false);
    const [educations, setEducationList] = useState<StudentEducation[]>([]);
    const studentApi = useContext(StudentApiContext);

    const closeWindow = () => {
        setOpenWindow(false);
        setReloadList(true)
    };

    const getEducations = () => {
        studentApi.getEducationListFromStudent()
            .then(studentEducations => {
                setEducationList(studentEducations);
            })
            .catch(err => alert(err));
    };

    const deleteEducation = (edId: string) => {
        studentApi.deleteEducation(edId)
            .then(res => console.log(res))
            .catch(err => alert(err))
    };

    useEffect(() => {
        setInterval(() => {
            getEducations()
        }, 1000);
    }, []);


    return (
        <Card className={classes.root}>
            <div>
                <IconButton onClick={(e) =>
                    setOpenWindow(true)
                }
                            size="large"
                            className={classes.buttons}>
                    <AddRoundedIcon style={{color: "#555d5c"}}/>
                </IconButton>
            </div>
            <section>
                {
                    educations.map((educations, index) => {
                        return (
                            <div>
                                <SimpleCard key={index}
                                            school={educations.school}
                                            field={educations.field}
                                            degree={educations.degree}
                                            city={educations.city}
                                            startDate={educations.startDate}
                                            endDate={educations.endDate}
                                />
                                <IconButton
                                    size="small"
                                    className={classes.buttons}
                                    key={index}
                                    onClick={() => {
                                        deleteEducation(educations.id)
                                    }}>
                                    <DeleteIcon
                                        style={{color: "#d70b44"}}
                                    />
                                </IconButton>
                            </div>
                        )
                    })
                }
            </section>
        </Card>
    )
}
