import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import student from '../assets/media/student.png';
import {Grid} from "@material-ui/core";
import {Link} from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'grid',
            '& > *': {
                margin: theme.spacing(2),
            }
        },
        img: {
            width: theme.spacing(18),
            height: theme.spacing(18),
            border: '0.1px solid lightgray'
        },
        position: {
            justifyContent: 'center',
            [theme.breakpoints.up('sm')]: {
                justifyContent: 'flex-start',
            }
        }
    })
);


export default function StudentImage() {
    const classes = useStyles();

    return (
        <Grid container className={classes.position}>
            <Grid item lg={1} md={1} sm={1}/>
            <Link to="/studentProfile">
                <Grid item className={classes.root}>
                    <Avatar className={classes.img} src={student} alt="Logo"/>
                </Grid>
            </Link>
        </Grid>
    );
}
