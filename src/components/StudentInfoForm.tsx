import React, {ChangeEvent, useContext, useState} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Paper, TextField} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";
import {StudentApiContext} from "../App";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            maxHeight: "80vh",
            width: "80%",
            margin: "20px",
            background: "#c7d8c6",
            justifyContent: "center"
        },
        form: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                justifyContent: "center",
                padding: "1%"
            }
        },
        formDesign: {
            display: "flex",
            justifyContent: "around",
            color: "#eeedec"
        },
        container: {
            textAlign: "right"
        },
        submitButton: {
            color: "#eeedec",
            marginTop: "10px"
        }
    })
);
type FormData = {
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    address: string;
    postcode: string;
    area: string;
    linkedin: string;
    gitlab: string;
    bio: string;

};
export default function StudentInfoForm() {
    const classes = useStyles();
    const {register, handleSubmit, formState: {errors}} = useForm<FormData>();
    const studentApi = useContext(StudentApiContext);

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [address, setAddress] = useState("");
    const [postcode, setPostcode] = useState("");
    const [area, setArea] = useState("");
    const [linkedin, setLinkedin] = useState("");
    const [gitlab, setGitlab] = useState("");
    const [bio, setBio] = useState("");


    const handleFirstNameChange = (event: ChangeEvent<HTMLInputElement>) => {
        let newFirstName = event.target.value;
        setFirstName(newFirstName);
    }
    const handleLastNameChange = (event: ChangeEvent<HTMLInputElement>) => {
        let newLastName = event.target.value;
        setLastName(newLastName);
    }
    const handleEmailChange = (event: ChangeEvent<HTMLInputElement>) => {
        let newEmail = event.target.value;
        setEmail(newEmail);
    }
    const handlePhoneChange = (event: ChangeEvent<HTMLInputElement>) => {
        let newPhoneNr = event.target.value;
        setPhone(newPhoneNr);
    }
    const handleAddressChange = (event: ChangeEvent<HTMLInputElement>) => {
        let newAddress = event.target.value;
        setAddress(newAddress);
    }
    const handlePostCodeChange = (event: ChangeEvent<HTMLInputElement>) => {
        let newPostNr = event.target.value;
        setPostcode(newPostNr);
    }
    const handleAreaChange = (event: ChangeEvent<HTMLInputElement>) => {
        let newArea = event.target.value;
        setArea(newArea);
    }
    const handleLinkedInChange = (event: ChangeEvent<HTMLInputElement>) => {
        let newLinkedIn = event.target.value;
        setLinkedin(newLinkedIn);
    }
    const handleGitChange = (event: ChangeEvent<HTMLInputElement>) => {
        let newGitLab = event.target.value;
        setGitlab(newGitLab);
    }
    const handleBioChange = (event: ChangeEvent<HTMLInputElement>) => {
        let newBio = event.target.value;
        setBio(newBio);
    }

    const onSubmit = handleSubmit((data) => {
        studentApi.updateStudent({
            email: data.email,
            firstName: data.firstName,
            lastName: data.lastName
        })
            .then(res => console.log(res))
            .catch(err => alert(err))
    });


    return (
        <Paper elevation={20} className={classes.root}>
            <form className={classes.form} noValidate autoComplete="off" onSubmit={onSubmit}>
                <div className={classes.formDesign}>
                    <TextField {...register("firstName")}
                               size="small"
                               id="outlined-name-input"
                               label="Förnamn"
                               value={firstName}
                               variant="outlined"
                               style={{width: "46%"}}
                               required
                               onChange={handleFirstNameChange}
                    />
                    <TextField {...register("lastName")}
                               size="small"
                               id="outlined-lastname-input"
                               label="Efternamn"
                               value={lastName}
                               variant="outlined"
                               style={{width: "46%"}}
                               required
                               onChange={handleLastNameChange}
                    />
                </div>
                <div className={classes.formDesign}>
                    <TextField {...register("email")}
                               size="small"
                               id="outlined-email-input"
                               label="E-mail"
                               value={email}
                               variant="outlined"
                               style={{width: "46%"}}
                               required
                               onChange={handleEmailChange}

                    />
                    <TextField {...register("phone")}
                               size="small"
                               id="outlined-email-input"
                               label="Telefonnummer"
                               value={phone}
                               variant="outlined"
                               style={{width: "46%"}}
                               required
                               onChange={handlePhoneChange}
                    />
                </div>
                <div className={classes.formDesign}>
                    <TextField {...register("address")}
                               size="small"
                               id="outlined-email-input"
                               label="Adress"
                               value={address}
                               variant="outlined"
                               style={{width: "41%"}}
                               required
                               onChange={handleAddressChange}
                    />
                    <TextField {...register("postcode")}
                               size="small"
                               id="outlined-email-input"
                               label="Postnr"
                               value={postcode}
                               variant="outlined"
                               style={{width: "28%"}}
                               required
                               onChange={handlePostCodeChange}
                    />
                    <TextField {...register("area")}
                               size="small"
                               id="outlined-email-input"
                               label="Ort"
                               value={area}
                               variant="outlined"
                               style={{width: "28%"}}
                               required
                               onChange={handleAreaChange}
                    />
                </div>
                <div className={classes.formDesign}>
                    <TextField {...register("linkedin")}
                               size="small"
                               id="outlined-email-input"
                               label="Linkedin"
                               value={linkedin}
                               variant="outlined"
                               style={{width: "46%"}}
                               required
                               onChange={handleLinkedInChange}
                    />
                    <TextField {...register("gitlab")}
                               size="small"
                               id="outlined-email-input"
                               label="Gitlab"
                               value={gitlab}
                               variant="outlined"
                               style={{width: "46%"}}
                               required
                               onChange={handleGitChange}
                    />
                </div>
                <TextField {...register("bio")}
                           size="small"
                           id="outlined-email-input"
                           label="BIO"
                           value={bio}
                           variant="outlined"
                           style={{width: "98%"}}
                           required
                           onChange={handleBioChange}
                />
                <div className={classes.container}>
                    <Button type="submit" className={classes.submitButton}>
                        SPARA
                    </Button>
                </div>
            </form>
        </Paper>
    )
}
