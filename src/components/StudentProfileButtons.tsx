import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import {Container, Grid} from "@material-ui/core";
import EducationButton from "./studentprofile/EducationButton";
import ExperienceButton from "./studentprofile/ExperienceButton";
import SkillsButton from "./SkillsButton";
import AboutButton from "./studentprofile/AboutButton";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            marginBottom: '1rem',
            [theme.breakpoints.up('sm')]: {
                marginBottom: '2rem'
            }
        }
    })
);


export default function StudentProfileButtons() {
    const classes = useStyles();

    return (
        <Container className={classes.root}>
            <Grid container spacing={1} justifyContent={'center'}>
                <AboutButton/>
                <ExperienceButton/>
                <EducationButton/>
                <SkillsButton/>
            </Grid>
        </Container>
    );
}
