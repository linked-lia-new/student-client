import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {Grid} from "@material-ui/core";
import StudentSkillScreen from "../screens/StudentSkillScreen";
import StudentWorkScreen from "../screens/StudentWorkScreen";
import StudentEducationScreen from "../screens/StudentEducationScreen";
import Ec from "../assets/media/Ec.png";
import Avatar from "@material-ui/core/Avatar";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            justifyContent: 'center',
            minWidth: 275,
            alignItems: 'center'
        },
        schoolImg: {
            marginLeft: "70%",
            marginBottom: "2%",
            width: theme.spacing(10),
            height: theme.spacing(10),
            border: '0.1px solid lightgray'
        },
        studentBackgroundColumn: {
            justifyContent: 'center',
            marginRight: 20,
            marginLeft: 20,
            marginBottom: 20,
            height: 1020
        },
        schoolInfoColumn: {
            justifyContent: 'center',
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 20,
            height: 310
        },
        studentSkillColumn: {
            justifyContent: 'center',
            marginLeft: 20,
            marginRight: 20,
            marginBottom: 20,
            height: 690
        },
        city: {
            marginBottom: 12,
        },
        about: {
            marginTop: 10,
            marginRight: 10,
            minHeight: 113
        },
    }),
);

export default function StudentProfileInfo() {
    const classes = useStyles();

    return (
        <Grid container className={classes.root}>
            <Grid item md={6} sm={12} xs={12}>
                <Card className={classes.studentBackgroundColumn} variant="outlined">
                    <CardContent>
                        <Typography variant="h5">
                            Edda Togård
                        </Typography>
                        <Typography className={classes.city} color="textSecondary">
                            Stockholm
                        </Typography>
                        <Typography className={classes.about} color="textSecondary">
                            Går sista läsåret på EC utbildnings program för Javautvecklare.
                            Jag har sedan flera år ett intresse av programmering och har tidigare arbetat en hel del med
                            front-end.
                            <br/>
                            Inför sista terminen söker jag nu en LIA-plats där jag kan fortsätta utvecklas som
                            programmerare.
                        </Typography>
                        <StudentEducationScreen/>
                        <StudentWorkScreen/>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item md={4} sm={12} xs={12}>
                <Card className={classes.schoolInfoColumn} variant="outlined">
                    <CardContent>
                        <Typography variant="subtitle1">
                            Utbildning: Javautvecklare
                            <br/>
                            Skola: EC Utbildning
                            <br/>
                            Tid för LIA:
                            <br/>
                            LIA1: 10 jan - 4 mars 2022
                            <br/>
                            LIA2: 7 mars - 13 maj 2022
                            <br/>
                            OPEN TO WORK
                        </Typography>
                    </CardContent>
                    <Grid item>
                        <Avatar className={classes.schoolImg} src={Ec} alt="Profilbild"/>
                    </Grid>
                </Card>
                <Card className={classes.studentSkillColumn} variant="outlined">
                    <CardContent>
                        <StudentSkillScreen/>
                    </CardContent>
                </Card>
            </Grid>
        </Grid>
    );
}
