import React, {ChangeEvent, useContext, useState} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Paper, TextField} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";
import {FormControl, InputLabel, Select, SelectChangeEvent} from "@mui/material";
import MenuItem from '@mui/material/MenuItem';
import {StudentApiContext} from "../App";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            minHeight: "38vh",
            width: "80%",
            margin: "20px",
            background: "#c7d8c6",
            justifyContent: "center"
        },
        form: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                width: '25ch'
            }
        },
        formDesign: {
            display: "flex",
            justifyContent: "around",
            color: "#eeedec"
        },
        container: {
            textAlign: "right"
        }
    })
);

type FormData = {
    skill: string;
    level: string;
};

export default function StudentSkillForm() {
    const classes = useStyles();
    const {register, handleSubmit, formState: {errors}} = useForm<FormData>();
    const [skill, setSkill] = useState("");
    const [level, setLevel] = useState("");
    const studentApi = useContext(StudentApiContext);


    const handleSkillChange = (event: ChangeEvent<HTMLInputElement>) => {
        let temp = event.target.value;
        setSkill(temp);
    }
    const handleLevelChange = (event: SelectChangeEvent) => {
        let temp = event.target.value;
        setLevel(temp);
    }

    const onSubmit = handleSubmit((data) => {
        studentApi.addSkill({skill: skill, level: level})
            .then(res => console.log(res))
            .catch(err => alert(err))
    });

    return (
            <Paper elevation={20} className={classes.root}>
                <form className={classes.form} noValidate autoComplete="off" onSubmit={onSubmit}>
                    <TextField {...register("skill")}
                               size="small"
                               id="outlined-name-input"
                               label="Kompetens"
                               value={skill}
                               variant="outlined"
                               style={{width: "46%"}}
                               required
                               onChange={handleSkillChange}
                    />
                    <FormControl style={{width: "46%"}}>
                        <InputLabel id="demo-simple-select-helper-label">Nivå</InputLabel>
                        <Select {...register("level")} style={{height: "40px", marginTop: "8px"}}
                                labelId="demo-simple-select-helper-label"
                                id="demo-simple-select-helper"
                                value={level}
                                label="Nivå"
                                onChange={handleLevelChange}>
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            <MenuItem value={"BEGINNER"}>Nybörjare</MenuItem>
                            <MenuItem value={"MODERATE"}>Medel</MenuItem>
                            <MenuItem value={"EXPERT"}>Expert</MenuItem>
                        </Select>
                    </FormControl>
                    <div className={classes.container}>
                        <Button type="submit" style={{color: "#eeedec", marginTop: "10px"}}>
                            LÄGG TILL
                        </Button>
                    </div>
                </form>
            </Paper>
    )
}
