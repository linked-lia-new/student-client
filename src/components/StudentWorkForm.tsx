import React, {ChangeEvent, useContext, useState} from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Paper, TextField} from "@material-ui/core";
import Button from '@material-ui/core/Button';
import {useForm} from "react-hook-form";
import {StudentApiContext} from "../App";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            minHeight: "38vh",
            width: "80%",
            margin: "20px",
            background: "#c7d8c6",
            justifyContent: "center"
        },
        form: {
            '& .MuiTextField-root': {
                margin: theme.spacing(1),
                width: '25ch'
            }
        },
        formDesign: {
            display: "flex",
            justifyContent: "around"
        },
        container: {
            textAlign: "right"
        },
        submitButton: {
            color: "#eeedec",
            marginTop: "10px"
        }
    })
);

type FormData = {
    company: string;
    position: string;
    city: string;
    description: string;
    startDate: string;
    endDate: string;
};

export default function StudentWorkForm() {
    const classes = useStyles();
    const {register, handleSubmit, formState: {errors}} = useForm<FormData>();

    const [company, setCompany] = useState("");
    const [position, setPosition] = useState("");
    const [city, setCity] = useState("");
    const [description, setDescription] = useState("");
    const [start, setStart] = useState("");
    const [end, setEnd] = useState("");
    const studentApi = useContext(StudentApiContext);


    const handleCompanyChange = (event: ChangeEvent<HTMLInputElement>) => {
        let company = event.target.value;
        setCompany(company);
    }
    const handlePositionChange = (event: ChangeEvent<HTMLInputElement>) => {
        let position = event.target.value;
        setPosition(position);
    }
    const handleCityChange = (event: ChangeEvent<HTMLInputElement>) => {
        let city = event.target.value;
        setCity(city);
    }
    const handleDescriptionChange = (event: ChangeEvent<HTMLInputElement>) => {
        let description = event.target.value;
        setDescription(description);
    }
    const handleStartChange = (event: ChangeEvent<HTMLInputElement>) => {
        let start = event.target.value;
        setStart(start);
    }
    const handleEndChange = (event: ChangeEvent<HTMLInputElement>) => {
        let end = event.target.value;
        setEnd(end);
    }

    const onSubmit = handleSubmit((data) => {
        studentApi.addExperience({
            company: company,
            position: position,
            city: city,
            description: description,
            startDate: start,
            endDate: end
        })
            .then(res => console.log("RESPONSE  ", res.company))
            .catch(err => alert(err))
    });

    return (
        <Paper elevation={20} className={classes.root}>
            <form className={classes.form} noValidate autoComplete="off" onSubmit={onSubmit}>
                <TextField {...register("company")}
                           size="small"
                           id="outlined-name-input"
                           label="Företag"
                           value={company}
                           variant="outlined"
                           style={{width: "46%"}}
                           required
                           onChange={handleCompanyChange}
                />
                <TextField {...register("position")}
                           size="small"
                           id="outlined-lastname-input"
                           label="Yrke"
                           value={position}
                           variant="outlined"
                           style={{width: "46%"}}
                           required
                           onChange={handlePositionChange}
                />
                <TextField {...register("city")}
                           size="small"
                           id="outlined-lastname-input"
                           label="Stad"
                           value={city}
                           variant="outlined"
                           style={{width: "46%"}}
                           required
                           onChange={handleCityChange}
                />
                <TextField {...register("description")}
                           size="small"
                           id="outlined-lastname-input"
                           label="Beskrivning"
                           value={description}
                           variant="outlined"
                           style={{width: "46%"}}
                           required
                           onChange={handleDescriptionChange}
                />
                <TextField {...register("startDate")}
                           size="small"
                           id="outlined-lastname-input"
                           label="Från"
                           value={start}
                           variant="outlined"
                           style={{width: "46%"}}
                           required
                           onChange={handleStartChange}
                />
                <TextField {...register("endDate")}
                           size="small"
                           id="outlined-lastname-input"
                           label="Till"
                           value={end}
                           variant="outlined"
                           style={{width: "46%"}}
                           required
                           onChange={handleEndChange}
                />
                <div className={classes.container}>
                    <Button type="submit" className={classes.submitButton}>
                        LÄGG TILL
                    </Button>
                </div>
            </form>
        </Paper>
    )
}
