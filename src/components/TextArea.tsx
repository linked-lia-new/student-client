import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Grid} from "@material-ui/core";

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        marginLeft: 20,
        marginRight: 10
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)'
    },
    title: {
        fontSize: 14
    },
    pos: {
        marginBottom: 12
    }
});

export default function TextArea() {
    const classes = useStyles();
    const bull = <span className={classes.bullet}>READ MORE</span>;

    return (
        <Grid item>
            <Card className={classes.root} variant="outlined">
                <CardContent>
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                        Om oss
                    </Typography>
                    <Typography variant="h5" component="h2">
                        Spotify
                    </Typography>
                    <Typography className={classes.pos} color="textSecondary">
                        We share music to the world.
                    </Typography>
                    <Typography variant="body2" component="p">
                        Gör din LIA hos oss!
                        <br/>
                        We are waiting..
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small">{bull}</Button>
                </CardActions>
            </Card>
        </Grid>
    );
}
