import * as React from 'react';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import giphy from '../assets/media/giphy.gif';
import {CardContent, Grid, IconButton, Typography} from "@material-ui/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlayCircle} from "@fortawesome/free-solid-svg-icons";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import TextArea from "./TextArea";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        playButton: {
            display: 'grid',
            justifyContent: 'center',
            zIndex: 1
        },
        position: {
            justifyContent: 'center',
            marginTop: 10,
            marginLeft: 10,
            [theme.breakpoints.up('sm')]: {
                justifyContent: 'flex-start',
            }
        },
        cardContent: {
            display: 'none',
            [theme.breakpoints.up('sm')]: {
                display: 'block'
            }
        }
    })
);

export default function VideoCard() {
    const classes = useStyles();

    return (
        <Grid container className={classes.position} spacing={5}>
            <Grid item>
                <Card sx={{maxWidth: 345}}>
                    <CardMedia
                        component="img"
                        alt='monkey'
                        image={giphy}
                    />
                    <Grid item className={classes.playButton}>
                        <IconButton onClick={() => {
                            window.open("https://www.youtube.com/watch?v=UGu1jVZjlLk", "_blank");
                        }}>
                            <FontAwesomeIcon icon={faPlayCircle}/>
                        </IconButton>
                    </Grid>
                    <CardContent>
                        <Typography gutterBottom variant="h5" component="div">
                            Praktisera hos oss!
                        </Typography>
                        <Typography variant="body2" className={classes.cardContent}>
                            Välkommna att ansöka LIA hos drömföretaget!
                            Vi är bäst på alla sätt. Finns inte så mycket mer att säga.
                            Vi ses snart! :)
                        </Typography>
                    </CardContent>
                </Card>
            </Grid>
            <Grid item md={3} sm={6} xs={12} style={{marginTop: 30}}>
                <TextArea/>
            </Grid>
        </Grid>
    );
}
