import * as React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Paper} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import SaveIcon from "@mui/icons-material/Save";
import ListItemCardCompany from "./ListItemCardCompany";

const useStyles = makeStyles({
    root: {
        maxHeight: "500px",
        width: "80%",
        padding: "5px",
        backgroundColor: "#eeedec",
        overflow: "hidden",
        overflowY: "scroll",
        margin: "20px",
        "&::-webkit-scrollbar": {
            width: 15,
        },
        "&::-webkit-scrollbar-track": {
            boxShadow: `inset 0 0 9px #c7d8c6`,
        },
        "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#c7d8c6",
        },
    },
    searchBar: {
        width: "80%",
        margin: "20px"
    },
    list: {
        justifyContent: "space-around",
        flex: "column",
        display: "flex",
        alignItems: "center"
    },
});

const ListCompany = () => {
    const companyList = [
        {name: "Spotify", place: "Stockholm", image: "Spotify"},
        {name: "Scania", place: "Södertälje", image: "scania"},
        {name: "IKEA", place: "malmo", image: "Ikea"},
        {name: "EC", place: "Solna", image: "Ec"},
        {name: "Google", place: "London", image: "Google"},
    ];

    const classes = useStyles();

    return (
        <div>
            <Paper elevation={3} className={classes.root}>
                <section>
                    {
                        companyList.map((company, index) => {
                            return (
                                <div className={classes.list}>
                                    <ListItemCardCompany key={index} images={company.image}
                                                         name={company.name}
                                                         place={company.place}
                                    />
                                    <IconButton size="large" style={{
                                        backgroundColor: "#c7d8c6", borderRadius: "50%",
                                        boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
                                    }}>
                                        <SaveIcon
                                            style={{color: "#726e6e"}}
                                        />
                                    </IconButton>
                                </div>
                            )
                        })
                    }
                </section>
            </Paper>
        </div>
    );
};

export default ListCompany;
