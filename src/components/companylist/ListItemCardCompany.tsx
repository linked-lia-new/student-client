import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import {Link} from 'react-router-dom';

const useStyles = makeStyles({
    root: {
        display: "flex",
        margin: "10px",
        marginLeft: 20,
        width: "90%",
        padding: "5px",
        justifyContent: "space-around",
        backgroundColor: "#c7d8c6",
        color: "#726e6e"
    },
    titleText: {
        color: "#726e6e",
        fontWeight: "bold",
        textDecoration: 'none',
        textAlign: 'left'
    },
    city: {
        color: '#726e6e',
        marginTop: 10,
        textDecoration: 'none',
        textAlign: 'left'
    },
    imgSize: {
        marginTop: 30,
        marginBottom: 30,
        marginRight: 40,
        marginLeft: 40,
        height: 60,
        width: 70,
        alignItem: "center"
    },
});

// @ts-ignore
const ListItemCardCompany = ({images, name, place}) => {
    const classes = useStyles();

    return (
        <Card elevation={5} className={classes.root}>
            <Link to="/companyProfile">
                <CardMedia className={classes.imgSize}
                           component="img"
                           alt="Company logo"
                           height="40"
                           width="70"
                           title="Company logo"
                           image={require(`../../assets/media/${images}.png`).default}
                />
            </Link>
            <CardActionArea>
                <Link to="/companyProfile">
                    <CardContent >
                        <Typography className={classes.titleText} gutterBottom variant="h4" component="h2">
                            {name}
                        </Typography>
                        <Typography variant="body2" className={classes.city}>
                            {place}
                        </Typography>
                    </CardContent>
                </Link>
            </CardActionArea>
        </Card>
    );
}

export default ListItemCardCompany;
