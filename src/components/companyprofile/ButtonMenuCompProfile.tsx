import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import {Container, Grid} from "@material-ui/core";
import MessageButton from "./MessageButton";
import WeblinkButton from "./WeblinkButton";
import TagButton from "./TagButton";
import LiaAddButton from "./LiaAddButton";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            marginBottom: '1rem',
            [theme.breakpoints.up('sm')]: {
                marginBottom: '4rem',
            },
        },
    }),
);

export default function ButtonMenuCompProfile() {
    const classes = useStyles();

    return (
        <Container className={classes.root}>
            <Grid container spacing={1} justifyContent={'center'}>
                <MessageButton/>
                <WeblinkButton/>
                <TagButton/>
                <LiaAddButton/>
            </Grid>
        </Container>
    );
}
