import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import {CardActionArea, Grid, Paper} from "@material-ui/core";
import {Link} from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        item: {
            marginLeft: 8,
            background: '#efd9c1',
            color: '#726e6e',
            textAlign: 'center',
            padding: 5
        },
    }),
);

export default function LiaAddButton() {
    const classes = useStyles();

    return (
        <Grid item md={2} sm={3} xs={12}>
            <Link to="/liaAdd" style={{textDecoration: 'none'}}>
                <CardActionArea>
                    <Paper className={classes.item}>
                        LIA annonser
                    </Paper>
                </CardActionArea>
            </Link>
        </Grid>
    );
}
