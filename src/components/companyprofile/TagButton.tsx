import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import {CardActionArea, Grid, Paper} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        item: {
            marginLeft: 8,
            background: '#efd9c1',
            color: '#726e6e',
            textAlign: 'center',
            padding: 5
        },
    }),
);

export default function TagButton() {
    const classes = useStyles();

    return (
        <Grid item md={2} sm={3} xs={12}>
            <CardActionArea>
                <Paper className={classes.item}>
                    Taggar
                </Paper>
            </CardActionArea>
        </Grid>
    );
}
