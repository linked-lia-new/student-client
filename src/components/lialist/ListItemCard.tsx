import React, {useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import OrionLondonlogo from '../../assets/media/OrionLondonlogo.png';
import {Link} from 'react-router-dom';
import IconButton from "@mui/material/IconButton";
import SaveIcon from "@mui/icons-material/Save";


const useStyles = makeStyles({
    root: {
        display: "flex",
        margin: "10px",
        marginLeft: 20,
        width: "90%",
        padding: "5px",
        justifyContent: "space-around",
        backgroundColor: "#c7d8c6",
        color: "#726e6e",
        textDecoration: 'none'
    },
    titleText: {
        color: "#726e6e",
        fontWeight: "bold",
        textDecoration: "none",
        textAlign: 'center'
    },
    companyName: {
        color: "#726e6e",
        fontWeight: "bold",
        textDecoration: "none",
        textAlign: 'center'
    },
    city: {
        color: '#726e6e',
        marginTop: 10,
        textDecoration: 'none',
        textAlign: 'center'
    },
    imgSize: {
        marginTop: 30,
        marginBottom: 30,
        marginRight: 40,
        marginLeft: 40,
        height: 60,
        width: 70,
        alignItem: "center"
    },
});

export default function ListItemCard() {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <Link to="/companyProfile">
                <CardMedia className={classes.imgSize}
                           component="img"
                           alt="Company logo"
                           image={OrionLondonlogo}
                           title="Company logo"
                />
            </Link>
            <CardActionArea>
                <Link to="/liaAdd">
                    <CardContent>
                        <Typography className={classes.titleText} gutterBottom variant="h5" component="h2">
                            Jobbtitel
                        </Typography>
                        <Typography variant="body2" className={classes.companyName}>
                            TEAM LIA
                        </Typography>
                        <Typography variant="body2" className={classes.city}>
                            Stockholm
                        </Typography>
                    </CardContent>
                </Link>
            </CardActionArea>
            <CardActions>
                <IconButton size="large" style={{
                    backgroundColor: "#c7d8c6", borderRadius: "50%",
                    boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
                }}>
                    <SaveIcon
                        style={{color: "#726e6e"}}
                    />
                </IconButton>
            </CardActions>
        </Card>
    );
}
