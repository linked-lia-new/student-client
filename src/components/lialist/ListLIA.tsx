import * as React from 'react';
import ListItemCard from "./ListItemCard";
import {makeStyles} from "@material-ui/core/styles";
import {Paper} from "@mui/material";

const useStyles = makeStyles({
    root: {
        maxHeight: "500px",
        width: "80%",
        margin: "5px",
        padding: "5px",
        backgroundColor: "#eeedec",
        overflow: "hidden",
        overflowY: "scroll",
        "&::-webkit-scrollbar": {
            width: 15,
        },
        "&::-webkit-scrollbar-track": {
            boxShadow: `inset 0 0 9px #c7d8c6`,
        },
        "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#c7d8c6",
        },
    },
    test: {
        backgroundColor: "#eeedec",
    },
});


export default function ListLIA() {
    const classes = useStyles();

    return (
        <div className={classes.test}>
            <Paper elevation={3} className={classes.root}>
                <ListItemCard/>
                <ListItemCard/>
                <ListItemCard/>
                <ListItemCard/>
                <ListItemCard/>
                <ListItemCard/>
                <ListItemCard/>
                <ListItemCard/>
            </Paper>
        </div>
    );
}
