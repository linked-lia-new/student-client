import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import {Grid} from "@material-ui/core";
import LiaDescription from "./LiaDescription";
import LiaCompanyDescription from "./LiaCompanyDescription";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root3: {
            display: 'grid',
            '& > *': {
                margin: theme.spacing(2),
            },
        },
        root: {
            justifyContent: 'center',
            minWidth: 275,
        },
    }),
);

export default function LiaAd() {
    const classes = useStyles();

    return (
        <Grid container className={classes.root}>
            <LiaDescription/>
            <LiaCompanyDescription/>
        </Grid>
    );
}
