import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import PopUpWindow from "../PopUpWindow";
import StudentInfoForm from "../StudentInfoForm";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        applyButton: {
            color: '#726e6e',
            background: '#c7d8c6',
            paddingRight: 40,
            paddingLeft: 40
        },
    }),
);

export default function LiaApplyButton() {
    const classes = useStyles();
    const [openWindow, setOpenWindow] = React.useState(false);
    const closeWindow = () => {
        setOpenWindow(false);
    }

    return (
        <CardActions>
            <Button variant="outlined" size="medium" className={classes.applyButton}
                    onClick={() => setOpenWindow(true)}>Ansök Här</Button>
            <PopUpWindow openWindow={openWindow} closeWindow={closeWindow}>
                <StudentInfoForm/>
            </PopUpWindow>
        </CardActions>
    )
}
