import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import {Grid} from "@material-ui/core";
import {Link} from "react-router-dom";
import Spotify from "../../assets/media/Spotify.png";
import Avatar from "@material-ui/core/Avatar";
import LiaApplyButton from "./LiaApplyButton";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root3: {
            display: 'grid',
            '& > *': {
                margin: theme.spacing(2),
            },
        },
        img: {
            width: theme.spacing(9),
            height: theme.spacing(9),
            border: '0.1px solid lightgray'
        },
        root2: {
            justifyContent: 'center',
            marginLeft: 10,
            marginRight: 10,
            marginBottom: 20
        },
        pos: {
            marginBottom: 12,
        },
    }),
);

export default function LiaCompanyDescription() {
    const classes = useStyles();

    return (
        <Grid item md={3} sm={3} xs={6}>
            <Card className={classes.root2} variant="outlined">
                <Link to="/companyProfile">
                    <Grid item className={classes.root3}>
                        <Avatar className={classes.img} src={Spotify} alt="Logo"/>
                    </Grid>
                </Link>
                <CardContent>
                    <Typography variant="h5" component="h2">
                        Spotify
                    </Typography>
                    <Typography className={classes.pos} color="textSecondary">
                        We share music to the world.
                    </Typography>
                    <Typography variant="body2" component="p">
                        Gör din LIA hos oss!
                    </Typography>
                </CardContent>
                <LiaApplyButton/>
            </Card>
        </Grid>
    );
}
