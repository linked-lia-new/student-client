import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Grid} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root1: {
            justifyContent: 'center',
            marginRight: 20,
            marginLeft: 20,
            marginBottom: 20
        },
        pos: {
            marginBottom: 12,
        },
        readButton: {
            color: "#726e6e",
            background: "#efd9c1",
        },
    }),
);

export default function LiaDescription() {
    const classes = useStyles();

    return (
        <Grid item md={6} sm={6} xs={12}>
            <Card className={classes.root1} variant="outlined">
                <CardContent>
                    <Typography variant="h5">
                        Software Developer
                    </Typography>
                    <Typography className={classes.pos} color="textSecondary">
                        Stockholm
                    </Typography>
                    <Typography variant="subtitle1">
                        Omfattning: Heltid
                        <br/>
                        Varaktighet: Tillsvidare
                        <br/>
                        Anställningsform: Tillsvidare- eller tidsbegränsad anställning
                        <br/>
                        <br/>
                    </Typography>
                    <Typography variant="body2" component="p">
                        Pellentesque consequat massa nulla, vitae gravida massa cursus ut. Maecenas blandit suscipit
                        augue et hendrerit. Aenean sit amet lorem lectus. Aenean vitae luctus mi. Donec eget ante id
                        dolor pulvinar auctor. Vivamus sit amet ultrices lacus. Morbi non odio quis orci scelerisque
                        lobortis finibus sed leo. Nam bibendum enim ut diam suscipit, at auctor nunc rutrum. Sed
                        congue condimentum augue, in fringilla augue luctus sed. Pellentesque habitant morbi
                        tristique senectus et netus et malesuada fames ac turpis egestas. Aenean ac vulputate velit.
                        Fusce vitae diam quam. Phasellus eu lectus magna. Praesent efficitur ligula leo, a placerat
                        erat venenatis at. Donec porta rutrum nibh id porttitor. Pellentesque vitae leo eu tortor
                        fermentum sagittis et a leo.
                        <br/>
                        <br/>
                        Ut interdum id enim non sodales. Nulla facilisi. Aliquam et quam eu mi pharetra lacinia non
                        vel tellus. Nam convallis erat laoreet nunc sollicitudin vulputate. Mauris blandit purus vel
                        elit blandit fringilla. Sed luctus viverra libero a fringilla. Suspendisse potenti.
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button variant="outlined" size="small" className={classes.readButton}>Läs Mer</Button>
                </CardActions>
            </Card>
        </Grid>
    );
}
