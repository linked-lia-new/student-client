import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import {CardActionArea, Grid, Paper} from "@material-ui/core";
import PopUpWindow from "../PopUpWindow";
import StudentEducationForm from "../StudentEducationForm";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        item: {
            marginLeft: 8,
            background: '#efd9c1',
            color: '#726e6e',
            textAlign: 'center',
            padding: 5,
            textDecoration: 'none',
        },
    }),
);

export default function EducationButton() {
    const classes = useStyles();
    const [openWindow, setOpenWindow] = React.useState(false);

    const closeWindow = () => {
        setOpenWindow(false);
    }

    return (
        <Grid item md={2} sm={3} xs={12}>
            <CardActionArea>
                <Paper className={classes.item} onClick={() => setOpenWindow(true)}>
                    UTBILDNING
                </Paper>
            </CardActionArea>
            <PopUpWindow openWindow={openWindow} closeWindow={closeWindow}>
                <StudentEducationForm/>
            </PopUpWindow>
        </Grid>
    );
}
