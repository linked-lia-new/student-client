import React, {useContext} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Card, Typography} from "@material-ui/core";
import {StudentApiContext} from "../../App";

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        margin: "10px",
        padding: "5px",
        width: "90%",
        justifyContent: "space-between",
        backgroundColor: "#c7d8c6",
        color: "#726e6e"
    }
}));

export default function ExperienceCard(props: any) {
    const classes = useStyles();
    const {company, position, city, description, start, end} = props;
    const studentApi = useContext(StudentApiContext);

    console.log("Start -- End ** ", start, " ", end);
    console.log("Company ** ", company);

    return (
        <Card elevation={5} className={classes.root}>
            <div>
                <Typography variant="subtitle1" gutterBottom>
                    {company}
                </Typography>
                <Typography variant="body2" gutterBottom>
                    {position}
                </Typography>
                <Typography variant="body2" gutterBottom component="div">
                    {city}
                </Typography>
                <Typography variant="body2" gutterBottom component="div">
                    {description}
                </Typography>
                <Typography variant="body2" gutterBottom component="div">
                    {start} -- {end}
                </Typography>
            </div>
        </Card>
    )
}
