import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import ImageList from '@material-ui/core/ImageList';
import ImageListItem from '@material-ui/core/ImageListItem';
import OrionLondonlogo from '../../assets/media/OrionLondonlogo.png';
import clipart1094777 from '../../assets/media/clipart1094777.png';
import {Grid} from "@material-ui/core";
import TextArea from "../TextArea";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        imageList: {
            flexWrap: 'nowrap',
            // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
            transform: 'translateZ(6)',

            "&::-webkit-scrollbar": {
                width: 7,
            },
            "&::-webkit-scrollbar-track": {
                boxShadow: `inset 0 0 9px #c7d8c6`,
            },
            "&::-webkit-scrollbar-thumb": {
                backgroundColor: "#c7d8c6",
            },
        },
        position: {
            justifyContent: 'center',
            [theme.breakpoints.up('sm')]: {
                justifyContent: 'center',
            }
        }
    }),
);

const itemData = [
    {
        img: OrionLondonlogo,
        title: '1'
    },
    {
        img: clipart1094777,
        title: '2'
    },
    {
        img: OrionLondonlogo,
        title: '3'
    },
    {
        img: clipart1094777,
        title: '4'
    },
];

export default function Gallery() {
    const classes = useStyles();

    return (
        <Grid container spacing={5}>
            <Grid item md={6} sm={6} xs={12}>
                <TextArea/>
            </Grid>
            <Grid item className={classes.root} xs={12} sm={6} lg={5}>
                <ImageList className={classes.imageList} cols={2}>
                    {itemData.map((item) => (
                        <ImageListItem key={item.img}>
                            <img
                                onClick={() => {
                                    alert('Öppna bilden! ' + item.title);
                                }}
                                src={item.img}
                                alt={'Image'}
                            />
                        </ImageListItem>
                    ))}
                </ImageList>
            </Grid>
        </Grid>
    );
}
