
export default interface StudentEducation{
    "id": string,
    "school": string,
    "field": string,
    "degree": string,
    "city": string,
    "startDate": string,
    "endDate": string,
}