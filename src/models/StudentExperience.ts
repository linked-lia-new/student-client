
export default interface StudentExperience{
    "id": string,
    "company": string,
    "position": string,
    "city": string,
    "description": string,
    "startDate": string,
    "endDate": string,
}
