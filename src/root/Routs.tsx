import React from 'react';
import {Route, Switch} from 'react-router-dom';
import StudentMainScreen from "../screens/StudentMainScreen";
import CompanyProfile from "../screens/CompanyProfile";
import SearchLia from "../screens/SearchLia";
import SearchCompany from "../screens/SearchCompany";
import LiaAddView from "../screens/LiaAddView";
import StudentProfile from "../screens/StudentProfile";
import StudentInfoForm from "../components/StudentInfoForm";
import StudentEducationScreen from "../screens/StudentEducationScreen";
import StudentWorkScreen from '../screens/StudentWorkScreen';
import StudentSkillScreen from '../screens/StudentSkillScreen';
import LogoutComponent from "../components/LogoutComponent";

function Routs() {
    return (
        <main>
            <Switch>
{/*             <Route path="/matches"/>
                <Route path="/messages"/>
                <Route path="/visitors"/>
                <Route path="/help"/>*/}
                <Route path="/studentProfile" component={StudentMainScreen}/>
                <Route path="/profile" component={StudentProfile}/>
                <Route path="/searchLia" component={SearchLia}/>
                <Route path="/companyProfile" component={CompanyProfile}/>
                <Route path="/searchCompany" component={SearchCompany}/>
                <Route path="/savedLia" component={SearchLia}/>
                <Route path="/appliedLia" component={SearchLia}/>
                <Route path="/liaAdd" component={LiaAddView}/>
                <Route path="/studentInfo" component={StudentInfoForm}/>
                <Route path="/studentEducation" component={StudentEducationScreen}/>
                <Route path="/studentWork" component={StudentWorkScreen}/>
                <Route path="/studentSkills" component={StudentSkillScreen}/>
                {/*<Route path="/tipsAndTricks"/>
                <Route path="/editLayout"/>
                <Route path="/contactAdmin"/>
                <Route path="/deleteAccount"/>*/}
                {/*<Route path="/welcomePage" component={StudentProfile}/>*/}
                <Route path="/logout" component={LogoutComponent}/>
            </Switch>
        </main>
    )
}

// <Route path="/profile" component={StudentMainScreen user=student} exact/>

export default Routs;
