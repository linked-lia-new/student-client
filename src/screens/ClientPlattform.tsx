import React, {useLayoutEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Navbar from "../components/Navbar";
import SideNavElements from "../components/SideNavElements"
import LogoPlaceholder from "../components/LogoPlaceholder";
import {useMediaQuery} from 'react-responsive';
import Routs from "../root/Routs";
import keycloak from "../keycloak";

const addNew = 0
const useStyles = makeStyles((theme) => ({
    appBar: {marginLeft: addNew}
}))

function ClientPlattform() {
    const isDesktopOrLaptop = useMediaQuery({query: '(min-width: 1224px)'})
    const isTabletOrMobile = useMediaQuery({query: '(max-width: 1224px)'})
    const targetRef = React.createRef<HTMLDivElement>()
    const [dimensions, setDimensions] = useState({width: 0, height: 0});
    const [data, setData] = useState(73);

    keycloak.onReady = authenticated => {
        if (!authenticated)
            window.location.href = "http://localhost:3000";
    }

    useLayoutEffect(() => {
        if (targetRef.current) {
            const boundingRect = targetRef.current.getBoundingClientRect()
            const {width, height} = boundingRect
            setDimensions({width: Math.round(width), height: Math.round(height)})
            setData(width);
        }
    }, []);

    return (
        <div className="Profile" style={{background: '#eeedec'}}>
            <header className="Profile-header">
                <div style={{height: '12vh'}}>
                    <LogoPlaceholder/>
                </div>
            </header>
            {isDesktopOrLaptop &&
            <div id='componentsContainer'>
                <div style={{paddingLeft: 217, position: 'fixed', width: '100vw', zIndex: 1,}}>
                    <Navbar/>
                </div>
                <div id='r00' style={{display: 'inline-block'}} ref={targetRef}>
                    <SideNavElements/>
                </div>
                <div style={{marginLeft: 217, marginTop: '64px'}}>
                    <Routs/>
                </div>
            </div>
            }
            {isTabletOrMobile &&
            <div id='componentsContainer'>
                <div style={{paddingLeft: 70, position: 'fixed', width: '100vw', zIndex: 1,}}>
                    <Navbar/>
                </div>
                <div id='r00' style={{display: 'inline-block'}} ref={targetRef}>
                    <SideNavElements/>
                </div>
                <div style={{marginTop: '64px'}}>
                    <Routs/>
                </div>
            </div>
            }
        </div>
    );
}

export default ClientPlattform;
