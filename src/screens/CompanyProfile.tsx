import React from 'react';
import ButtonMenuCompProfile from "../components/companyprofile/ButtonMenuCompProfile";
import LogoImage from "../components/companyprofile/LogoImage";
import Gallery from "../components/studentprofile/Gallery";
import {Grid} from "@material-ui/core";
import VideoCard from "../components/VideoCard";
import CompanyBigImage from '../components/companyprofile/CompanyBigImage';

export default function CompanyProfile() {

    return (
        <Grid container>
            <CompanyBigImage/>
            <LogoImage/>
            <ButtonMenuCompProfile/>
            <Gallery/>
            <VideoCard/>
        </Grid>
    );
}
