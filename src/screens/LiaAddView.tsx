import React from 'react';
import ButtonMenuLiaAdd from "../components/liaprofile/ButtonMenuLiaAdd";
import LogoImage from "../components/companyprofile/LogoImage";
import LiaAd from "../components/liaprofile/LiaAd";
import {Grid} from "@material-ui/core";
import LiaAddImage from "../components/liaprofile/LiaAddImage";

export default function LiaAddView() {

    return (
        <Grid container>
            <LiaAddImage/>
            <LogoImage/>
            <ButtonMenuLiaAdd/>
            <LiaAd/>
        </Grid>
    );
}
