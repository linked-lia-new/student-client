import React from 'react';
import SearchBar from "material-ui-search-bar";
import {makeStyles} from "@material-ui/core/styles";
import {Autocomplete, Box, TextField} from "@mui/material";
import ListLIA from "../components/lialist/ListLIA";
import ListCities from "../components/ListCities";

const useStyles = makeStyles({
    root: {
        background: "#eeedec",
        padding: "0",
        margin: "0"
    },
    searchBar: {
        width: "80%",
        margin: "20px",
        marginTop: "20px"
    },
});

const cities = ListCities();

export default function SearchLia() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Autocomplete className={classes.searchBar}
                          id="country-select-demo"
                          sx={{width: 300}}
                          options={cities}
                          autoHighlight
                          getOptionLabel={(option) => option.label}
                          renderOption={(props, option) => (
                              <Box component="li" sx={{'& > img': {mr: 2, flexShrink: 0}}} {...props}>
                                  <img
                                      loading="lazy"
                                      width="20"
                                      alt=""
                                  />
                                  {option.label}
                              </Box>
                          )}

                          renderInput={(params) => (
                              <TextField
                                  {...params}
                                  label="Välj stad"
                                  inputProps={{
                                      ...params.inputProps,
                                      autoComplete: 'new-password', // disable autocomplete and autofill
                                  }}
                              />
                          )}
            />
            <SearchBar className={classes.searchBar}/>
            <ListLIA/>
        </div>
    );
}
