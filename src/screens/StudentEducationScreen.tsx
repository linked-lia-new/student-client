import React, {useContext, useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Card} from "@material-ui/core";
import IconButton from "@mui/material/IconButton";
import SimpleCard from "../components/SimpleCard";
import {StudentApiContext} from "../App";
import StudentEducation from "../models/StudentEducation";
import DeleteIcon from "@mui/icons-material/Delete";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: "20px",
        marginBottom: "20px",
        margin: "5px",
        padding: "5px",
        backgroundColor: "#eeedec",
        overflow: "hidden",
        overflowY: "scroll",
        minHeight: 380,
        maxHeight: 380,
        "&::-webkit-scrollbar": {
            width: 15
        },
        "&::-webkit-scrollbar-track": {
            boxShadow: `inset 0 0 9px #c7d8c6`,
        },
        "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#c7d8c6"
        }
    },
    list: {
        justifyContent: "space-around",
        flex: "column",
        display: "flex",
        alignItems: "center"
    },
    title: {
        textAlign: 'center',
        color: "#726e6e",
        textDecoration: 'underline'
    }
}));

export default function StudentEducationScreen() {
    const classes = useStyles();
    const [educationList, setEducationList] = useState<StudentEducation[]>([]);
    const studentApi = useContext(StudentApiContext);

    const getEducations = () => {
        studentApi.getEducationListFromStudent()
            .then(studentEducations => {
                setEducationList(studentEducations);
            })
            .catch(err => alert(err));
    };

    const deleteEducation = (edId: string) => {
        studentApi.deleteEducation(edId)
            .then(res => console.log(res))
            .catch(err => alert(err))
    };

    useEffect(() => {
        setInterval(() => {
            getEducations()
        }, 1000);
    }, []);


    return (
        <Card className={classes.root}>
            <Typography className={classes.title} variant="h5">
                Min utbildning
            </Typography>
            <section>
                {
                    educationList.map((educations, index) => {
                        return (
                            <div className={classes.list}>
                                <SimpleCard key={index} school={educations.school} field={educations.field}
                                            degree={educations.degree}
                                            city={educations.city} startDate={educations.startDate}
                                            endDate={educations.endDate}
                                />
                                <IconButton
                                    size="small"
                                    style={{
                                        backgroundColor: "#c7d8c6", borderRadius: "50%",
                                        boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
                                    }} key={index} onClick={() => {
                                    deleteEducation(educations.id)
                                }}>
                                    <DeleteIcon
                                        style={{color: "#726e6e"}}
                                    />
                                </IconButton>
                            </div>
                        )
                    })
                }
            </section>
        </Card>
    )
}
