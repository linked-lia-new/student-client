import React from 'react';
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import {Paper, Tab, Tabs} from "@material-ui/core";
import StudentAddFileForm from "../components/StudentAddFileForm";
import StudentInfoForm from "../components/StudentInfoForm";
import StudentWorkScreen from "./StudentWorkScreen";
import StudentEducationScreen from "./StudentEducationScreen";
import StudentSkillScreen from "./StudentSkillScreen";
import PopUpWindow from "../components/PopUpWindow";
import StudentResumeScreen from "./StudentResumeScreen";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        try: {
            width: "70%",
            textAlign: "center"

        },
        studenttab: {
            margin: "20px",
            marginBottom: "0px",
            background: "#ffffff",
            justifyContent: "space-between",
        },
    }),
);
export default function StudentMainScreen() {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const [openWindow, setOpenWindow] = React.useState(false);
    const handleChange = (event: any, newValue: number) => {
        setValue(newValue);
    };

    const closeWindow = () => {
        setOpenWindow(false);
    }

    function getPaper() {
        return <Paper elevation={20} className={classes.studenttab}>
            <Tabs value={value} onChange={handleChange} style={{backgroundColor: "#cccbc6"}}>
                <Tab label="Info"/>
                <Tab label="Education"/>
                <Tab label="Experience"/>
                <Tab label="Skills"/>
                <Tab label="Resume"/>
            </Tabs>
            {value === 0 && <StudentInfoForm/>}
            {value === 1 && <StudentEducationScreen/>}
            {value === 2 && <StudentWorkScreen/>}
            {value === 3 && <StudentSkillScreen/>}
            {value === 4 && <StudentResumeScreen/>}
        </Paper>;
    }

    return (
        <div className={classes.try}>
            {getPaper()}
            <PopUpWindow openWindow={openWindow} closeWindow={closeWindow}>
                <StudentAddFileForm/>
            </PopUpWindow>
        </div>
    )
}
