import React from 'react';
import {Grid} from "@material-ui/core";
import StudentImage from '../components/StudentImage';
import StudentBackground from "../components/StudentBackground";
import StudentProfileInfo from '../components/StudentProfileInfo';
import StudentChangeImageButton from "../components/StudentChangeImageButton";
import StudentProfileButtons from "../components/StudentProfileButtons";

export default function StudentProfile() {

    return (
        <Grid container>
            <StudentBackground/>
            <StudentImage/>
            <StudentChangeImageButton/>
            <StudentProfileButtons/>
            <StudentProfileInfo/>
        </Grid>
    );
}
