import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Card} from "@material-ui/core";
import IconButton from "@mui/material/IconButton";
import AddRoundedIcon from '@mui/icons-material/AddRounded';
import SimpleCard from "../components/SimpleCard";
import PopUpWindow from "../components/PopUpWindow";
import StudentAddFileForm from "../components/StudentAddFileForm";

const useStyles = makeStyles((theme) => ({
    root: {
        margin: "20px",
        padding: "10px",
        backgroundColor: "#efd9c1"
    }
}));

export default function StudentResumeScreen() {
    const classes = useStyles();
    const [openWindow, setOpenWindow] = React.useState(false);

    const closeWindow = () => {
        setOpenWindow(false);
    }

    return (
        <Card className={classes.root}>
            <div>
                <IconButton onClick={(e) => setOpenWindow(true)} size="large"
                            style={{backgroundColor: "#ddb6a2", marginRight: "5px", borderRadius: "50%"}}>
                    <AddRoundedIcon style={{color: "#555d5c"}}/>
                </IconButton>
            </div>
            <SimpleCard company="Shakir" position="Resume" from="" to=""/>
            <PopUpWindow title="Resume" openWindow={openWindow} closeWindow={closeWindow}>
                <StudentAddFileForm/>
            </PopUpWindow>
        </Card>
    )
}