import React, {useContext, useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Card} from "@material-ui/core";
import IconButton from "@mui/material/IconButton";
import StudentSkill from '../models/StudentSkill';
import {StudentApiContext} from "../App";
import DeleteIcon from "@mui/icons-material/Delete";
import SkillCard from "../components/SkillCard";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
    root: {
        margin: "5px",
        padding: "5px",
        backgroundColor: "#eeedec",
        overflow: "hidden",
        overflowY: "scroll",
        height: 640,
        "&::-webkit-scrollbar": {
            width: 15,
        },
        "&::-webkit-scrollbar-track": {
            boxShadow: `inset 0 0 9px #c7d8c6`,
        },
        "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#c7d8c6",
        }
    },
    list: {
        justifyContent: "space-around",
        flex: "column",
        display: "flex",
        alignItems: "center"
    },
    title: {
        textAlign: 'center',
        color: "#726e6e",
        textDecoration: 'underline'
    }
}));

export default function StudentSkillScreen() {
    const classes = useStyles();
    const [skillList, setSkillList] = useState<StudentSkill[]>([]);
    const studentApi = useContext(StudentApiContext);

    const getSkills = () => {
        studentApi.getSkillListFromStudent()
            .then(skills => {
                setSkillList(skills);
            })
            .catch(err => alert(err));
    };

    const deleteSkill = (skillId: string) => {
        studentApi.deleteSkill(skillId)
            .then(value => console.log("Deleted Skill ", value))
            .catch(err => alert(err));
    };

    useEffect(() => {
        setInterval(() => {
            getSkills();
        }, 1000);
    }, []);

    return (
        <Card className={classes.root}>
            <Typography className={classes.title} variant="h5">
                Mina kompetenser
            </Typography>
            <section>
                {
                    skillList.map((skills, index) => {
                        return (
                            <div className={classes.list}>
                                <SkillCard key={index}
                                           skill={skills.skill}
                                           level={skills.level}
                                />
                                <IconButton size="small"
                                            style={{
                                                backgroundColor: "#c7d8c6",
                                                borderRadius: "50%",
                                                boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
                                            }} key={index} onClick={() => {
                                    deleteSkill(skills.id)
                                }}>
                                    <DeleteIcon
                                        style={{color: "#726e6e"}}
                                    />
                                </IconButton>
                            </div>
                        )
                    })
                }
            </section>
        </Card>
    )
}
