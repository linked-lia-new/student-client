import React, {useContext, useEffect, useState} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Card} from "@material-ui/core";
import IconButton from "@mui/material/IconButton";
import ExperienceCard from "../components/studentprofile/ExperienceCard";
import {StudentApiContext} from "../App";
import StudentExperience from "../models/StudentExperience";
import DeleteIcon from "@mui/icons-material/Delete";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
    root: {
        margin: "5px",
        padding: "5px",
        backgroundColor: "#eeedec",
        overflow: "hidden",
        overflowY: "scroll",
        minHeight: 380,
        maxHeight: 380,
        "&::-webkit-scrollbar": {
            width: 15
        },
        "&::-webkit-scrollbar-track": {
            boxShadow: `inset 0 0 9px #c7d8c6`
        },
        "&::-webkit-scrollbar-thumb": {
            backgroundColor: "#c7d8c6"
        }
    },
    list: {
        justifyContent: "space-around",
        flex: "column",
        display: "flex",
        alignItems: "center"
    },
    title: {
        textAlign: 'center',
        color: "#726e6e",
        textDecoration: 'underline'
    }
}));

export default function StudentWorkScreen() {
    const classes = useStyles();
    const [experienceList, setExperienceList] = useState<StudentExperience[]>([]);
    const studentApi = useContext(StudentApiContext);

    const getExperience = () => {
        studentApi.getExperienceListFromStudent()
            .then(studentExperience => {
                setExperienceList(studentExperience);
            })
            .catch(err => alert(err));
    };

    const deleteExp = (expId: string) => {
        studentApi.deleteExperience(expId)
            .then(res => console.log(res))
            .catch(err => alert(err))
    };

    useEffect(() => {
        setInterval(() => {
            getExperience()
        }, 1000);
    }, []);

    return (
        <Card className={classes.root}>
            <Typography className={classes.title} variant="h5">
                Min erfarenhet
            </Typography>
            <section>
                {
                    experienceList.map((experiences, id) => {
                        return (
                            <div className={classes.list}>
                                <ExperienceCard key={id}
                                                company={experiences.company}
                                                position={experiences.position}
                                                city={experiences.city}
                                                description={experiences.description}
                                                start={experiences.startDate}
                                                end={experiences.endDate}
                                />
                                <IconButton size="small"
                                            style={{
                                                backgroundColor: "#c7d8c6",
                                                borderRadius: "50%",
                                                boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)"
                                            }}
                                            key={id} onClick={() => {
                                    deleteExp(experiences.id)
                                }}>
                                    <DeleteIcon
                                        style={{color: "#726e6e"}}
                                    />
                                </IconButton>
                            </div>
                        )
                    })
                }
            </section>
        </Card>
    )
}
